# Budget Committee

## Purpose

Manage the budget of the Haskell Foundation

## Responsibilities

1. Propose annual budgets for the board’s consideration
1. Determine when changes are needed to the budget
1. Advise the board on budgetary matters

### Deliverables
* Yearly budget, continuously up to date
* Budget publication policy, one time delivery plus change management as needed
* Budget guidelines, one time delivery plus change management as needed
* A process involving the Executive Director on regularly reporting on income and spending forecasts

### Approach

Generally, budgetary measures will be proposed by the Executive Director to the Budget Committee,
which will review and give initial feedback.  Once the Budget Committee approves the budget, it
will propose the budget to the entire board for final approval.

## Delegated powers

* Access to all financial data and related documents that may or may not be visible to all Board
  members by default.

## Term

This is a standing (permanent) committee

## Membership

1. Ryan Trinkle (chair)
1. José Pedro Magalhães (vice chair)
1. Wendy Devolder
1. Tom Ellis

## Membership Rules

The Treasurer and the Vice Treasurer of the Haskell Foundation must be members and are
typically Chair and Vice Chair of this committee.

Changes to the list of members requires simple majority Board approval.

## Voting Procedure

The voting procedure of this committee is the same as the procedure for the
Board itself.

## Reporting

The Committee will report to the Board at least monthly, either by a briefing
at a Board meeting or, if that is not possible, by an email update to the
Board.

## Documents

The Committee stores its working documents and deliverables in a subfolder
named [Budget](https://drive.google.com/drive/u/0/folders/1c92IT6bQyJI0xfS10vtsMu4w8253Nh5l)
in the *Committees* folder in the Haskell Foundation Google Drive folder.

## Committee Bylaws

Any change to this charter requires a vote of the Board, with two exceptions:

* The Committee may change its method of document storage and repositories.

Any exercise of these exceptions must be accompanied by a notification to the
Board in a Board meeting or by email.
