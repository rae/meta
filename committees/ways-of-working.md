# Ways of Working Committee

The Ways of Working Committee is a committee of the Haskell Foundation Board.

## Purpose

The purpose of the Ways of Working committee is to write the Foundation's rules of self governance, specifically
* [The Haskell Foundation Board Ways of Working document](https://gitlab.haskell.org/hf/meta/-/blob/main/board.md)
* [The Haskell Foundation Communications Guidelines document](https://gitlab.haskell.org/hf/meta/-/blob/main/communication.md)
* The Haskell Foundation Bylaws document (tbd)

The committee's remit includes both the legally binding bylaws and the informal rules that we agree to abide to.

## Responsibilities
Drive the board towards defining all of its rules of self governance.

## Delegated powers
None.

This committee is delivering proposals for the board to vote on.

## Term
This committee has a finite lifetime: it will close once all open questions of self governance have been answered and ratified by the Board.
See the [project plan](https://docs.google.com/spreadsheets/d/1iG_HrYbaoQGodPioMINhtS9YxrRwNwI8roxuPlIylDI/edit?usp=drive_web&ouid=118260674851061511230) for details.

We deliver a sequence of individual "chapters". This will yield tangible progress early, and also allow the other board members to digest and get used to our bylaws and ways of working in manageable chunks. Also, we don't block the important topics with the long tail.

We track the status here in this [project plan](https://docs.google.com/spreadsheets/d/1iG_HrYbaoQGodPioMINhtS9YxrRwNwI8roxuPlIylDI/edit#gid=0).

## Membership

* Chair: Richard Eisenberg
* Other members:
    * Ryan Trinkle
    * Alexander Bernauer

## Membership Rules

The Chair may unilaterally add and remove members, but is expected
to do so in consultation with others and must notify the Board. To this end,
the Chair is entitled to unilaterlay complete merge requests that alter only
the list of members of this committee.

The members may elect a new Chair with two-thirds majority at any time.
The new Chair must notify the Board in that case.

All members including the Chair otherwwise serve without term limits, other
than the terms of the committee itself.

If you would like to contribute, please reach out to the Chair.

## Voting Procedure

This committee agrees on draft versions unanimously. These drafts then get voted on by the Board.

## Reporting

The Committee will report to the Board at least monthly, either by a briefing
at a Board meeting or, if that is not possible, by an email update to the
Board.

## Documents

The Committee stores its working documents and deliverables in a subfolder
named Ways of Working in the *Committees* folder in the Haskell
Foundation Google Drive folder ([here](https://drive.google.com/drive/folders/12dyZcbZGEf6ZPcARJY81z-gM-9EkoTOb)).

The final deliverables are merged to [the HF Gitlab repo](https://gitlab.haskell.org/hf/meta/-/tree/main).

## Committee Bylaws

Any change to this charter requires a vote of the Board, with two exceptions:

* The Committee may change its method of document storage and repositories.
* The Committee may make changes to its membership according to the rules set
out in the *Membership Rules* section above.

Any exercise of these exceptions must be accompanied by a notification to the
Board in a Board meeting or by email.
