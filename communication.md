# Communication

This document describes how the Haskell Foundation Board communicates
among itself and to the broader Haskell community.

In thinking about communication, it is helpful to identify (at least) four different scenarios:
1. Board members communicating with one another
2. Community members talking to the Board
3. The Board communicating out to the community
4. Community members talking with one other (including Board members, who are a part of the community).

This document refers back to these scenarios by number.

## Guidelines

The Board needs to be a functioning, cohesive group that collectively drives the Foundation's mission.
To that end, we have recognised that the communication guidelines below are helpful for avoiding accidental
miscommunication for when Board members communicate with one another.

These guidelines are not a list of requirements, nor a procedural mechanism that guarantees to avoid
miscommunication. But they serve as a mental checklist for things to consider when communicating. Use your
best judgement.

* **Avoid assumptions**: We all come from different backgrounds, have different knowledge, and have had different experiences. 
  When making assumptions about the others' state of mind, there is a risk of misunderstanding and miscommunication.
  We all make assumptions very naturally in order to operate at all. Still, it's advisable to pause for a second,
  and if in doubt, double check with your peer whether your assumption holds or not. On the flipside, recognise that if your
  peer has failed to validate an assumption with you, this most probably was a simple oversight without ill intent. In such a 
  situation, it is best to approach them and sort it out in good faith.

* **Reach out early**: If you see situations you are concerned about -- or you don't understand but think you should -- do
  not hesitate to reach out to your peers early. Maybe there is a hidden assumption. Maybe others haven't noticed yet.
  Or maybe others disagree. Either way, we can only make progress as a group if we aling on these things, in particularly
  if they have the potential to escalate.

* **Choose the right level of detail**: When addressing the group as a whole, avoid too-much-information but summarise the
  key points instead. Focus on providing relevant context, calling out assumptions, an providing an overview of what is going
  on and/or what you expect from your peers. In contrast, when interacting within a more tactical environemnt, e.g., a 
  committee or one-to-one, do work out the details required to make progress.

  As a concrete example, here is a practical template for whenever you address the board for decision
  making. Bolding text can be useful here for the part that is most important for individuals to read.
  Also, we suggest to highlight the beginning of each section.

    1. Concise statement of the problem being solved
    1. Concise statement of the proposed solution (or multiple options, if the author is asking for
      a decision rather than approval)
    1. Discussion (details, analysis, and motivation)
    1. Call to action (please vote, specific deadline, etc.)

* **Keep the group informed**: Our effectiveness to steer the Foundation and manage all activities
  around it depends on our respective ability to develop fully-informed opinions and have situational
  awareness. To this end, it is advisable that we  keep this group informed about relevant topics.
  The most relevant topics are those that may come up as a request for vote to the board. General
  what-is-going-on updates are welcome as well.


## Asynchronous communication

1. Internal communication within the HF (Scenario 1) and from members of the community specifically addressing the Board
   (Scenario 2) go via a [mailing list](https://groups.google.com/a/haskell.foundation/g/board).

   a. The main mailing list of the Board is `board@haskell.foundation`. This
   list reaches all Board members plus the Executive Team.

   b. Other sub-groups may establish their own lists, which they may advertise as
      appropriate.

   c. Archives of the `board@haskell.foundation` list are [public](https://groups.google.com/a/haskell.foundation/g/board).

   d. The public may write emails
   to the Board by writing to the mailing list address directly, or (if the writer has
   a Google account) via the web interface
   available on the archive page.
   Such emails will be moderated, but relevant ones (that is, not spam) will be delivered.

   e. Certain sensitive topics (e.g. personnel) are communicated by direct email
   to the Board members; these emails are not archived.

1. Communication from the Board to the community (Scenario 3) and between members of the community (Scenario 4)
   go via the [Haskell Foundation category](https://discourse.haskell.org/c/haskell-foundation/11)
   of the [Haskell Discourse instance](https://discourse.haskell.org/).

   a. Announcements from the Foundation (Scenario 3) will be posted there.

   b. Community members can also discuss topics of interest to the Haskell Foundation there.
   This might be, for example, to get early feedback on a proposal
   idea.

   c. Members of the HF will be sure to monitor the traffic in this Category.

1. All task forces, committees and ad hoc groups are invited to use
   the Haskell Foundation chat service (details TBD), because it is great for
   lightweight, private and informal conversations.

   a. In recognition of the fact that not all Board members will be able to
   monitor chat at all times, anyone participating in a chat discussion is
   encouraged, when it seems appropriate, to @mention any Board members
   they know of that may be particularly interested, or @channel for everyone,
   to invite their comments.

   b. Chat is used for informal debate, discussion, development, but not for
   *decisions*. For decisions, we use more formal, transparent, and archived
   mechanisms, including email, Board agendas, minutes, GitLab pull requests,
   and so on.

   c. All notifications of formal actions to be considered or taken by the
   Board shall be sent by email to all Board members, so that all Board
   members have an immutable record of the actions taken.
   
   d. We will endeavor to create systems of communication such that Board
   members can configure email notifications for all significant discussions,
   regardless of the medium of discussion. Therefore, the only medium Board
   members are *required* to monitor shall be email.
   
   e. All input to any Board resolution, outside of the email proposing the
   resolution, must be either a Gitlab merge request or a Google Drive
   document. This excludes links to a chat conversation, which is not
   archived.
   
   f. While our policy of respectful communication and our code of conduct
   apply, chat is considered private and is not subject to our transparency
   policy. Chat conversations are not archived.

1. Official communication between Board members goes through the mailing list. For example, an asynchronous
   vote is conducted only via the mailing list, never in other modes. Board members
   are *not* required to keep up with Discourse, and so if you want to reach the entire
   Board, email `board@haskell.foundation`.

   A good guideline for choosing between the mailing list and Discourse is:

   * To address the Board specifically (Scenario 2), email `board@haskell.foundation`.
   * To address the community (Scenario 4), including Board members, use Discourse.
   
## Synchronous communication (meetings)

The rules of official Board meetings (Scenario 1) are laid out [here](board.md).

* All Board meetings are posted on the official Haskell Foundation Google calendar. The public
   may subscribe to the calendar ID `g5pug6rj631f31d4ief6avdodc@group.calendar.google.com`
   or via [this calendar URL](https://calendar.google.com/calendar/u/1?cid=ZzVwdWc2cmo2MzFmMzFkNGllZjZhdmRvZGNAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ),
   or may [view this calendar](https://calendar.google.com/calendar/embed?src=g5pug6rj631f31d4ief6avdodc%40group.calendar.google.com).

* Agendas are posted in the appropriate folder in Google Drive.

* After the meeting, the agenda is made public (Scenario 3) by being added to the
   [minutes repository](https://gitlab.haskell.org/hf/minutes).

## Storage

1. The Haskell Foundation maintains a [Google Drive
   folder](https://drive.google.com/drive/folders/1gxc2miCWM0gwMoA7ywUH3qY--q277Ifc?usp=sharing),
   a [GitLab group](https://gitlab.haskell.org/hf), and a [GitHub organization](https://github.com/haskellfoundation/). As general guidelines: Google Drive is good
   for co-editing documents (Scenario 1); GitLab is good for long-term archiving and public
   release (Scenario 3); and GitHub is good for repos that expect a good deal of external
   contribution.

1. Both Google Drive and GitLab are intended to be used for Board-internal communication,
   communication with and within the Executive Team, communication within Task Forces as well
   general communication of the Foundation. (Scenarios 1 and 3)

1. The Board will establish access policies for different parts of its Google Drive and GitLab
   which the Chair and the Secretary will implement in their role as adminstrators.

1. The Executive Team establishes their own practices for data storage and document management
   and informs the Board about those.

### Current status

The GitLab group currently hosts these repositories:

* `meta`: Details of how the Haskell Foundation conducts its business. World-readable.

* `minutes`: Ratified minutes of official meetings. World-readable.




The GitHub organization currently hosts these repositories:

* `haskell.foundation-redux`: This is the repo for the newly designed website. World-readable.
