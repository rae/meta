This file is a template for charters to be used to form Board Committees. See
Section 8 of our [board.md](board.md) document for more details.

When editing this template, look for braces and replace or remove all phrases
in the braces.

---------------------------------------

# {Name} Committee

The {name} Committee is a committee of the Haskell Foundation Board.

## Purpose

{write purpose here}


## Responsibilities

{write responsibilities here}


## Delegated powers

{write delegated powers here; there may be none}


## Term

{Is this a standing committee or a tactical one? If tactical, what condition
causes the committee to conclude?}


## Membership

{Who is on the committee? Who is the Chair? Who are the SMEs?}


## Membership Rules

*If there are no delegated powers:* The Chair may unilaterally add and remove
members, but is expected to do so in consultation with others and must
notify the Board.

*If there are delegated powers:* Membership changes must be approved by a vote
of the Board.

{edit as necessary}


## Voting Procedure

The voting procedure of this committee is the same as the procedure for the
Board itself.

{edit as necessary}


## Reporting

The Committee will report to the Board at least monthly, either by a briefing
at a Board meeting or, if that is not possible, by an email update to the
Board.

{edit as necessary}


## Documents

The Committee stores its working documents and deliverables in a subfolder
named {Name of the Committee} in the *Committees* folder in the Haskell
Foundation Google Drive folder.

{edit as necessary}

{include any Committee-driven GitLab/GitHub repos here}


## Committee Bylaws

Any change to this charter requires a vote of the Board, with two exceptions:

* The Committee may change its method of document storage and repositories.
* The Committee may make changes to its membership according to the rules set
out in the *Membership Rules* section above.

Any exercise of these exceptions must be accompanied by a notification to the
Board in a Board meeting or by email.

{edit as necessary}
